# IP核

实验时可能会用到的参考IP库：<https://gitlab.com/hit-coa/ip_cores>

IP列表：

* Arithmetic logic unit IP core
* Branch target buffer IP core
* Cache tag dictionary IP core
* Data Cache storage memory block IP core
* Dual port general purpose register IP core
* Floating point arithmetic unit IP core
* MIPS32 instruction system decoder IP core
* Memory block IP core
* Read-only instruction cache IP core
* Readable and writable data cache IP core
