# 流水线控制模式

```{note}
下面是对于流水线的控制模式的简要介绍，可以参考，**本实验不要求使用**。
```

## 流水线的控制模式概述

流水线需要暂停，当某个流水线段被阻塞时，前面的流水段也需要被阻塞。这是因为：当某个流水段暂停时，此流水段没有办法接受新的数据，前面的流水段必须把原本的数据保持在本流水线段中，否则，如果数据继续向后流动，就会造成数据丢失，在没有特殊机制处理的情况下，就会发生错误。

控制流水线的阻塞，**核心在于控制好各级流水线段间寄存器的写使能信号**。

## 流水线级间互锁机制

### 核心思想

核心思想：为每一级流水线分配一个监管人员。它和前后级流水线的监管人员相互沟通情况（前级流水段：是否有数据传入；后级流水段：是否可以接受数据），决定下一时刻是否传送数据。如果后继流水段不可以接受数据，而且此时该流水段中有有效数据保存，就不可以接受前级流水段的数据输入。流水线中个流水段环环相扣，使得整个流水线正常运行。

### 例程代码

这是一个具有三段流水段的例程，其相邻流水级之间仅有如图所示的四个信号：

* `stageX_allowin`：流水段X传递给流水段X-1的信号，该信号有效说明下一拍流水段X可以接受流水段X-1的数据。
* `stageX_bus_r`：流水段X-1和流水段X之间的段间寄存器传递给流水线X的数据信号，该数据信号为上一流水级X-1传递给当前流水级X的数据，但未经过当前流水级X的处理。
* `stageX_to_stageX+1_valid`：流水段X传递给流水段X+1的信号，该信号有效说明此时流水段X有数据需要在下一周期传递给流水段X+1。
* `stageX_to_stageX+1_bus`：流水段X之间的段间寄存器传递给流水线X+1的数据信号，该信号为流水段X传递给流水段X+1的数据。

```{image} ../_static/img/labs/pipeline_distribute.jpg
:width: 100%
:align: center
```

此处，仅仅给出包含 stage2 的 pipeline2.v 模块。

````{note}
在 pipelineX.v 模块中，将包含 stageX 和 stageX前的段间寄存器。
```{image} ../_static/img/labs/pipeline_module_distribute.jpg
:width: 100%
:align: center
```
````

```verilog
//pipeline2.v
module pipeline_2 (
    input   clk                       ,
    input   rst                       ,
    //需要在流水段间传递的控制信号
    input   stage3_allowin_in         ,
    output  stage2_allowin_out        ,
    //来自于pipeline2的信号
    input   stage1_to_stage2_valid_in ,
    input   stage1_to_stage2_bus_in   ,
    //pipeline2传递到pipeline3的信号
    output  stage2_to_stage3_valid_out,
    output  stage2_to_stage3_bus_out
);
    //*******************************************************************
    //                  stage1 和 stage2 之间段间寄存器
    //*******************************************************************
    reg stage2_valid_r;
    reg stage2_bus_r;

    wire stage2_allowin;
    wire stage2_ready_go;
    wire stage2_to_stage3_valid;

    assign stage2_ready_go        = /*TODO*/;
    assign stage2_allowin         = !stage2_valid_r || stage2_ready_go && stage3_allowin_in;
    assign stage2_to_stage3_valid = stage2_valid_r && stage2_ready_go;

    /*
    *   stage2_ready_go信号说明：
    *       描述stage2当前拍状态的信号，该信号有效说明此时stage2的处理任务已经完成，可以向后一流水段传递数据。
    *       由于在部分流水段中，单个周期可能无法完成该阶段的信号转换，故而设置了该信号。
    *   stage2_ready_go信号赋值说明：
    *       TODO部分需要填充的是stage2的信号完成标志信号，如果可以确定当前流水段一定在单周期内完成信号转换，则可恒置为1。
    *   
    *   stage2_allowin信号说明：
    *       stage2传递给stage1的信号，该信号有效说明下一拍stage2可以接受stage1的数据。
    *   stage2_allowin信号赋值说明：
    *       如果当前流水段为null（段间寄存器内不存在任何数据），那么该信号置为有效。
    *       如果当前流水段不为null（段间寄存器内存在数据），且已知该流水段的信号已经处理完毕，且下一流水段可以接受数据，那么该信号置为有效。
    *
    *   stage2_to_stage3_valid信号说明：
    *       stage2传递给stage3的信号。该信号有效说明此时stage2有数据需要在下一周期传递给stage3。
    *   stage2_to_stage3_valid信号赋值说明：
    *       如果当前流水段不为null（段间寄存器内存在数据），且已知该流水段的信号已经处理完毕，那么该信号置为有效
    */
    always @(posedge clk) begin
        if (rst) begin
            stage2_valid_r <= 0;
        end else if (stage2_allowin) begin
            stage2_valid_r <= stage1_to_stage2_valid_in;
        end

        if (stage2_allowin && stage1_to_stage2_valid_in) begin
            stage2_bus_r <= stage1_to_stage2_bus_in;
        end
    end

    //*******************************************************************
    //                             stage2 
    //*******************************************************************

    //TODO:需要完成的信号转换 stage2_bus_r >> stage2_to_stage3_bus

    //*******************************************************************
    //                           输出信号
    //*******************************************************************
    assign stage2_allowin_out         = stage2_allowin;
    assign stage2_to_stage3_valid_out = stage2_to_stage3_valid;
    assign stage2_to_stage3_bus_out   = stage2_to_stage3_bus;

endmodule
```

## 流水线集中控制机制

### 核心思想

通俗意义上来讲，核心思想为：**配备一个生产流水线监管人员**。可以同时观察到各级流水线的状态，并向各级流水线下达命令。一旦某一时刻某个流水段阻塞，就立即给出前面流水段停止向后传送数据的命令。

### 例程代码

这是一个具有三段流水段的例程，除了流水级以及段间寄存器之外，还存在一个控制模块ctrl，该模块控制流水线段间寄存器的读写，其信号说明如下：

* `stall_from_X`：每一级流水线发送到总控制模块ctrl的控制信号。该信号高有效说明，下一周期时，流水段X的处理任务未完成，不可以向流水段X+1传递数据，需要暂停。
* `stall[X]`：总控制模块ctrl发送到每一级流水线的控制信号。该信号高有效说明，下一拍时，流水段X需要暂停。

```{image} ../_static/img/labs/pipeline_centrol.jpg
:width: 100%
:align: center
```

此处，仅仅给出包含 stage2 的 pipeline2.v 模块 以及 ctrl.v 模块。

````{note}
在 pipelineX.v 模块中，将包含 stageX 和 stageX前的段间寄存器。
```{image} ../_static/img/labs/pipeline_module_centrol.jpg
:width: 100%
:align: center
```
````

```verilog
//ctrl.v
module ctrl (
    input        stall_from_1_in,
    input        stall_from_2_in,
    input        stall_from_3_in,
    output [2:0] stall_out
);

    assign stall_out = (stall_from_3_in) ? 3'b111 : // stage3 暂停，{ stage1，stage2，stage3 }均暂停
                       (stall_from_2_in) ? 3'b011 : // stage2 暂停，{ stage1，stage2 }均暂停
                       (stall_from_1_in) ? 3'b001 : // stage1 暂停，{ stage1 }均暂停
                                           3'b000;  // 无暂停

endmodule
```

```verilog
//pipeline_2.v
module pipeline_2(
    input       clk                     ,
    input       rst                     ,

    input [2:0] stall_in                ,

    input       stage1_to_stage2_bus_in ,
    output      stage2_to_stage3_bus_out,
    output      stall_from_2_out
);

    //*******************************************************************
    //                  stage1 和 stage2 之间段间寄存器
    //*******************************************************************
    reg stage2_bus_r;

    /*
    * stall_in[x]高有效，stall_in[x+1]低有效：流水段X发生暂停，流水段X+1正常向后流动，需要将二者之间的段间寄存器置为0。
    * stall_in[x]高有效，stall_in[x+1]高有效：流水段X+1或流水段X+1之后的流水段发生暂停，不应改变二者之间寄存器的值。
    * stall_in[x]低有效，stall_in[x+1]低有效：流水线正常流动。
    * stall_in[x]低有效，stall_in[x+1]高有效：不可能出现。
    */
    always @(posedge clk) begin
        if (rst) begin
            stage2_bus_r <= 0;
        end else if ( stall_in[1] && !stall_in[2]) begin
            stage2_bus_r <= 0;
        end else if (!stall_in[1]) begin
            stage2_bus_r <= stage1_to_stage2_bus_in;
        end
    end

    //*******************************************************************
    //                             stage2 
    //*******************************************************************

    //TODO:需要完成的信号转换 stage2_bus_r >> stage2_to_stage3_bus

    //*******************************************************************
    //                           输出信号
    //*******************************************************************
    assign stall_from_2_out         = /*TODO*/;
    assign stage2_to_stage3_bus_out = stage2_to_stage3_bus;

endmodule
```
