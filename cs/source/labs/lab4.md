# 实验四：Cache的补全

## 实验目的

* 掌握 Vivado 集成开发环境
* 掌握 FPGA 编程方法及硬件调试手段
* 深刻理解 Cache 的映像规则、查找方法和替换算法
* 深刻理解LRU缓存替换策略

## 实验环境

* Vivado 集成开发环境和龙芯 Artix-7 实验平台

## 实验内容

根据课程第八章所讲的存储系统的相关知识，理解指令 Cache 的设计，并补全已有代码cache.v 中的三处空缺。需要补全的内容分别为：

* **填写 cache 状态转换自动机**：依据 [Cache 的自动机的状态描述](#Cache 的自动机的状态描述)一节内容，补全 Cache 状态转换自动机的代码。
* **补全 LRU 算法**：选路算法使用 LRU 算法，即选择近期最少被访问的块作为被替换的块。由于是二路 组相联，因此使用 1 位的 LRU 位来记录最近的访问信息。 要求补全 LRU 选路和更新算法，保证 Cache 正常工作的同时还需要确保 LRU 算法的有效性。
* **完成 cache 目录表和数据块的布线**：Cache 的查找是通过目录表来实现的。为了便于修改 Cache 的相联度，实验所用的目 录表和数据存储器由可扩展的 generate for 语法生成。要求将已经完成赋值的信号线正确地 连接到目录表 tagv 和数据存储器 data 上。

### 指令 Cache 的设计

本实验要求补全的是一个 2 路组相联的指令 Cache，只进行读操作，容量为 2 * 4KB， 使用 LRU 算法进行选路。存储器按字节寻址，寻址所使用的地址长度为 32 位，指令字长 也为 32 位。

指令 Cache 的行数为 128 行，因此索引需要 7 位，即 $2^7 = 128$；每行由 8 个块构成， 每个块 32 位（4 个字节），因此偏移量需要 5 位，即$2^5 = 8 * 4$；地址其余20位作为索引。 所以Cache中地址的结构如下所示：

<!-- {reg:[
  {bits: 5,  name: 'offset', type: 4, attr: 'address[4:0]'},
  {bits: 7,  name: 'index' , type: 2, attr: 'address[11:5]'},
  {bits: 20, name: 'tag'   , type: 3, attr: 'address[31:12]'}
] , config: {hspace: 800, vspace: 80}} -->

```{image} ../_static/img/labs/cache.png
:width: 90%
:align: center
```

指令 Cache 由目录表 TagV 模块和数据存储器 Data 模块构成。在 TagV 模块中，每路 中的每行都需要一位有效位，用于指示当前行的内容是否有效，所以单路的 TagV 模块由 一块 128 * 21-bit 的 BRAM 构成。在 Data 模块中一行需要存储 8 个数据块，每块 32 位，因此单路的 Data 模块由一块 128 * 256-bit 的 BRAM 构成。

该 Cache 支持两流水段的流水访存，即连续命中情况下，可以连续返回数据。为了实 现 Cache 的流水化，需要将 Cache 的访问划分为多个阶段。一般来讲，会将 Cache 的访问 划分为以下六个阶段：

* **Look up**：判断是否在 Cache 中，并读出 Data 供命中时返回读数据。
* **Hit Update**：命中 Cache 时，更新信息。
* **Miss handle**：从主存取回 Miss 的 Cache 行。
* **Select**：选出被替换的 Cache 行。
* **Replace**：将某一 Cache 行读出来，后续会替换出去。
* **Refill**：将新取回的 Cache 行填入 Replace 后剩余的空行中。

上述阶段并不是流水阶段的划分，有些阶段可能包含 2 个以上流水段，有些阶段也可 能共同存在于一个流水段里。需要根据 Cache 自动机的设计进行实际处理。

### Cache 的自动机的状态描述

Cache常用有限状态机（FSM）实现。下面是实验所用的Cache的自动机的状态描述：

```{list-table}
:header-rows: 1
:widths: 12 88

* - 状态
  - 描述
* - IDLE
  - 空转状态，Cache恢复正常工作前的缓冲状态，会在下一个状态转移到运行状态。
* - RUN
  - 运行状态，在这个状态下Cache如果不发生数据缺失就可以正常返回数据，发生数据缺失则会进行状态转移，在之后的状态中使用LRU算法进行Cache的替换和更新。
* - SEL_WAY
  - 选路状态，如果Cache发生数据缺失，就会进入这一状态，并根据LRU算法进行选路，为接下来Cache的替换和更新做准备。
* - MISS
  - 缺失状态，更新LRU算法相关的数据，并向主存储器发起读数据请求，请求读取Cache中未命中的行，如果从设备（主存储器）接受读请求，则Cache进入到接收状态接收返回的数据。
* - REFILL
  - 接收状态，Cache接收主存储器传回的数据，状态持续到数据传输完成。
* - FINISH
  - 结束状态，标志数据传输完成，Cache将在下一个状态回到空转状态。
* - RESETN
  - 初始化状态，用于处理器初始化时清空cache，持续128个时钟周期。
```

要求在理解Cache的工作原理后，补全Cache状态转换自动机的代码。

### 补全LRU算法

选路算法使用LRU算法，即选择近期最少被访问的块作为被替换的块。由于是二路组相联，因此使用1位的LRU位来记录最近的访问信息。

要求补全LRU选路和更新算法，保证Cache正常工作的同时还需要确保LRU算法的正确性。

### 完成cache目录表和数据块的布线

Cache的查找是通过目录表来实现的。为了便于修改Cache的相联度，实验所用的目录表和数据存储器由可扩展的generate for语法生成。要求将已经完成赋值的信号线正确地连接到目录表tagv和数据存储器data上。

## 实验要求

按照以上要求将给出的代码补全，并在仿真环境下通过给定测试。

### 实验预习

在实验开始前阅读并理解给出的指令 Cache 的代码，并理解下列内容：

* Cache 的映像规则，这个指令 Cache 是如何实现二路组相联的
* Cache 的查找算法，这个指令 Cache 是如何进行数据查找的
* Cache 的替换算法，这个指令 Cache 在数据缺失时是如何使用 LRU 算法进行 选路和替换的
* 理解 Verilog 的 generate 语法，这个指令 Cache 是如何使用目录表和数据块的

### 完成实验内容

对给出的 Verilog 语言代码 cache.v 进行补全，并在仿真环境下通过给定的测试。

```{note}
* 仅仅修改`cache.v`的代码缺失部分，不允许对 testbench 的代码部分和 trace 文件进行任何修改。
* 测试无需上板，仅仅在仿真环境中通过即可。
* 实验给出的Cache 通过sram-like接口与CPU交互，通过AXI接口与存储器交互，但实验中无需过于关注接口，仅需关注Cache即可。
```

## 实验测试环境

请到该网页下载：

[lab-env/lab4](https://gitlab.com/hit-coa/lab-env/-/tree/master/lab4)

## 参考资料

对于相连度很高的Cache，使用LRU算法需要更多的存储空间记录访问历史，因此实践中L1 Cache常用近似算法Pseudo-LRU，对于4路组相联Cache，Pseudo-LRU所需空间是LRU的一半。

1. CPU缓存. 维基百科，自由的百科全书：<https://zh.wikipedia.org/wiki/CPU%E7%BC%93%E5%AD%98>
2. Cache replacement policies. Wikipedia: <https://en.wikipedia.org/wiki/Cache_replacement_policies>
3. Pseudo-LRU. Wikipedia: <https://en.wikipedia.org/wiki/Pseudo-LRU>
