# 实验二：分支预测

## 实验目的

- 掌握 Vivado 集成开发环境
- 掌握 Verilog 语言
- 掌握 FPGA 编程方法及硬件调试手段
- 深刻理解动态分支预测的原理与方法

## 实验环境

* Vivado 集成开发环境和龙芯 Artix-7 实验平台

## 实验内容

* 根据课程第五章 MIPS 指令集的基本实现，设计并实现一个符合实验指令的**非流水**处理器， 包括 Verilog 语言的实现和 FPGA 芯片的编程实现，要求该处理器可以**通过所提供的自动测试环境**。

###  处理器功能

本实验的任务是设计一个简单的RISC处理器，该处理器是在给定的指令集（与MIPS32类似）下构建的，支持11条指令。假定存储器分为数据缓冲存储器和指令缓冲存储器，且都可以在一个时钟周期内完成一次同步存取操作，时钟信号和CPU相同。处理器的指令字长为32位，包含32个32位通用寄存器R0~R31，1个32位的指令寄存器IR和1个32位的程序计数器PC，1个256×32位指令缓冲存储器，1个256×32位的数据缓冲存储器。

###  指令系统定义

处理器所支持的指令包括`LW`，`SW`，`ADD`，`SUB`，`AND`，`OR`，`XOR`，`SLT`，`MOVZ`，`BEQ`，`J`。

其中仅有`LW`和`SW`是字访存指令，所有的存储器访问都通过这两条指令完成；`ADD`、`SUB`、`AND`、`OR`、`XOR`、`SLT`、`MOVZ`是运算指令，他们都在处理器内部完成；`BEQ`是分支跳转指令，根据寄存器的内容进行相对跳转；`J`是无条件转移指令。

#### 指令说明

关于汇编指令的说明：
* `rs`意味着source register，`rd`意味着destination register；
* `rt`有时（如运算指令）意味着第二个source register，有时（如访存指令）意味着target (source/destination) register；
* 我们使用 **[rs]** 表示寄存器`rs`的内容；

另外，所有算数运算指令都执行有符号运算。

#### 运算指令

（1）加法指令 `ADD rd, rs, rt`

```{image} ../_static/img/labs/instruction/add.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相加，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] + [rt]**

（2）减法指令 `SUB rd, rs, rt`

```{image} ../_static/img/labs/instruction/sub.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相减，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] - [rt]**

（3）与运算指令 `AND rd, rs, rt`

```{image} ../_static/img/labs/instruction/and.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相与，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] & [rt]**

（4）或运算指令 `OR rd, rs, rt`

```{image} ../_static/img/labs/instruction/or.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相或，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] | [rt]**

（5）异或指令 `XOR rd, rs, rt`

```{image} ../_static/img/labs/instruction/xor.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相异或，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] ⊕ [rt]**

（6）小于指令 `SLT rd, rs, rt`

```{image} ../_static/img/labs/instruction/slt.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相比较，结果决定目的寄存器的值。

具体为： **[rd] <- [rs] < [rt] ? 1 : 0**

（7）条件移动指令 `MOVZ rd, rs, rt`

```{image} ../_static/img/labs/instruction/movz.png
:width: 90%
:align: center
```

该指令根据其中一个源寄存器内容，决定另一个源寄存器的值是否写回目的寄存器。

具体为： **if ([rt] == 0) then [rd] <- [rs]**

```{note}
注意：当`[rt] != 0`时，不对寄存器`rd`进行任何写回操作。
```

#### 访存指令

```{warning}
使用访存指令时，计算后的目标地址必须是访问数据单元大小的整数倍。
对于LW和SW指令， ([base] + offset) % 4 == 0
```

（1）存数指令 `SW rt, offset(base)`

```{image} ../_static/img/labs/instruction/sw.png
:width: 90%
:align: center
```

该指令将寄存器`rt`的内容存于主存单元中，对应的地址由16位偏移地址`offset`经符号拓展加上`base`的内容生成。

具体操作为： **Mem[[base] + offset] <- [rt]**

（2）取数指令 `LW rt, offset(base)`

```{image} ../_static/img/labs/instruction/lw.png
:width: 90%
:align: center
```

该指令将主存单元中的内容存于寄存器`rt`，对应的地址由16位偏移地址`offset`经符号拓展加上`base`内容生成。

具体操作为： **[rt] <- Mem[[base] + offset]**

#### 转移类指令

```{warning}
* 和MIPS32指令集不同，我们给出的跳转指令没有延迟槽。
* 跳转指令使用NPC（也就是PC+4）参与跳转地址运算。
```

（1）条件转移（相等跳转）指令 `BEQ rs, rt, offset`

```{image} ../_static/img/labs/instruction/beq.png
:width: 90%
:align: center
```

该指令根据寄存器`rs`和`rt`的内容决定下一条指令的地址，若两个寄存器内容相等，则16位偏移`offset`扩充为32位，左移2位后与NPC相加，作为下一条指令的地址，否则程序按原顺序执行。

具体操作为： **PC <- ([rs] == [rt]) ? [sign_extend(offset) << 2 + NPC] : NPC**

（2）无条件转移指令 `J target`

```{image} ../_static/img/labs/instruction/j.png
:width: 90%
:align: center
```

该指令改变下一条指令的地址，地址由指令中的26位形式地址`instr_index`左移2位作为低28位，和NPC的高4位拼接而成。

具体操作为： **PC <- (NPC[31:28]) ## (instr_index << 2)**

## 实验要求

要求根据以上给定的指令系统设计处理器，包括指令格式设计、操作的定义、Verilog语言的实现及 FPGA 编程实现。处理器设计实验要求按指定阶段进行。

###  实验预习

在实验开始前给出处理器的**设计方案**，设计方案要求包括：

* 指令格式设计
* 处理器结构设计框图（要求精确到信号以及信号的位宽）及功能描述
* 各功能模块结构设计框图及功能描述
* 各模块输入输出接口信号定义（以表格形式给出）

### 完成实验内容

**Verilog 语言实现**：要求采用结构化设计方法，用 Verilog 语言实现处理器的设计。设计包括：

  * 各模块的详细设计（包括各模块功能详述，设计方法，Verilog 语言实现等）
  * 各模块的功能测试（每个模块作为一个部分，包括测试方案、测试过程和测 试波形等）
  * 系统的详细设计（包括系统功能详述，设计方法，Verilog 语言实现等）
  * 系统的功能测试（包括系统整体功能的测试方案、测试过程和测试波形等）

**FPGA 编程下载**：将比特流下载到 Artix-7 实验板中。然后利用 Vivado 的上板调试功能观察 Artix-7 实验板的 FPGA 芯片中的实际运行，观察处理器内部运行状态，显式输出内部状态运行结果。

**对处理器进行功能测试，记录运行过程和结果，完成实验报告**：对处理器进行功能测试，使用提供的处理器功能测试环境完成处理器功能测试，并观察记录运行过程和结果，完成实验报告。

## 处理器测试环境

为了帮助各位同学更好地完成实验，实验二要求使用我们提供的处理器测试环境对CPU进行测试，处理器测试环境包括仿真测试和板上测试两部分。

### 使用要求

要求每个同学的CPU对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module cpu (
    input            clk             ,  // clock, 100MHz
    input            resetn          ,  // active low

    // debug signals
    output [31:0]    debug_wb_pc     ,  // 当前正在执行指令的PC
    output           debug_wb_rf_wen ,  // 当前通用寄存器组的写使能信号
    output [4 :0]    debug_wb_rf_addr,  // 当前通用寄存器组写回的寄存器编号
    output [31:0]    debug_wb_rf_wdata  // 当前指令需要写回的数据
);

    /*TODO：完成非流水CPU的设计代码*/

endmodule
```

```{warning}
* 只允许按照给定的接口格式去设计CPU，不允许更改接口格式
* 所有信号在时钟上升沿采样，在写使能为0时，`debug_rf_addr`、`debug_rf_wdata`可以为任意值
* 复位后，尽量保证上述接口信号不出现X或Z
* 仅需添加待完成的CPU，其他部分不要修改
```

### 测试用例

针对实验二的处理器测试环境，我们提供了一个简单的测试用例对CPU进行测试，包含了要求的11种指令。

### 仿真结果

当所实现的CPU功能正确时，会在控制台打印**PASS**，如下图所示。

```{image} ../_static/img/labs/success.png
:width: 500px
:align: center
```

当所实现的CPU出现错误时，会在控制台打印错误信息，如下图所示。

其中 Reference 一行打印的是正确的CPU的输出信息，Error 一行打印的是你所实现的CPU的输出信息。

```{image} ../_static/img/labs/error.png
:width: 500px
:align: center
```

### 上板结果

CPU通过仿真验证后，需要进行将设计下载到 Artix-7 开发板上观察设计正确性，当所实现的CPU功能正确时，开发板上的一排单色LED灯会同时亮起，如下图所示。

```{image} ../_static/img/labs/on_board_success.png
:alt: on_board_success
:width: 100%
:align: center
```

<br>此外，需要同学们抓取debug_wb_pc、debug_wb_rf_addr、debug_wb_rf_wdata以及debug_wb_rf_wen四个信号，观察上板时这些信号的波形图。上板抓取信号的具体方法见**第3章 Vivado使用说明**中的`3.2.6 FPGA 在线调式说明`。

## 实验帮助信息

### 龙芯 Artix-7 实验板的使用

这里简单介绍一下本课程实验中如何使用提供的龙芯 Artix-7 实验板。

以下是实验板的实景图以及配套器件。

```{image} ../_static/img/labs/fpga.png
:width: 100%
:align: center
```

```{image} ../_static/img/labs/fpga_device.png
:width: 100%
:align: center
```

<br>在本课程实验中，我们只需用到龙芯 Artix-7 实验板、USB数据线以及电源适配器。在进行上板实验前，使用电源适配器将实验板与电源连接，并打开实验板电源开关，接着使用USB数据线将实验板与电脑连接，然后即可在Vivado中连接上实验板进行上板测试。

### 关于数据通路的复用

在进行CPU的总体设计时，需要着重考虑的一点是“**数据通路的复用**”。

毫无疑问，指令系统中的每条指令都需要一条数据通路来完成该指令的功能，不同指令的数据通路之间存在重合部分。如果设计出来的CPU的每条指令所需要的数据通路都不存在重合，那么该CPU浪费的资源多，性能也不好。

### 通用寄存器的设计

在单发射顺序CPU中，通用寄存器只需要两个读端口和一个写端口。通常使用异步读，同步写的时序。

```verilog
module regfile(
    input         clk   ,
    input         rst   ,
    // READ PORT 1
    input  [4 :0] raddr1,
    output [31:0] rdata1,
    // READ PORT 2
    input  [4 :0] raddr2,
    output [31:0] rdata2,
    // WRITE PORT
    input         wen   ,   //write enable, active high
    input  [4 :0] waddr ,
    input  [31:0] wdata
);
    reg [31:0] rf [31:1];

    // initial with $readmemh is synthesizable here
    initial begin
        $readmemh("path/to/reg_init_file", rf);
    end

    //WRITE
    always @(posedge clk) begin
        if (wen && |waddr) begin // don't write to $0
            rf[waddr] <= wdata;
        end
    end

    //READ OUT 1
    assign rdata1 = (raddr1 == 5'b0) ? 32'b0 : rf[raddr1];

    //READ OUT 2
    assign rdata2 = (raddr2 == 5'b0) ? 32'b0 : rf[raddr2];

endmodule
```
