# 实验四：指令 Cache 的设计与实现

## 实验目的

* 掌握 Vivado 集成开发环境
* 掌握 Verilog 语言
* 掌握 FPGA 编程方法及硬件调试手段
* 深刻理解指令 Cache 的结构和整体工作原理

## 实验环境

* Vivado 集成开发环境

## 实验内容

根据课程第八章所讲的存储系统的相关知识，自行设计一个指令cache，并使用 Verilog 语言实现之。要求设计的指令Cache可以是2路或4路，每路128行，每行32字节，最终实现的cache能够**通过所提供的自动测试环境**。

### 指令Cache各模块及工作过程的介绍

指令Cache介于 CPU 和主存之间。其使用类 Sram 接口与 CPU 进行通信，当 CPU 需要使用数据时，将 Cache 内缓存的数据发送给 CPU；使用 AXI 接口与主存进行通信，当 Cache 发生不命中时，将主存中的数据替换到 Cache 的数据块内。因此，要实现一个指令 Cache ，一方面我们需要知道其内部的构成（在本节中将会详细阐述），另一方面我们需要知道 Cache 和 CPU 以及 AXI 总线是如何相互配合工作的（在后两节`实现类Sram协议`和`实现AXI协议`中会详细阐述）。

如下图所示，一个简单的指令Cache内部分主要由两级流水段和控制模块构成，本次实验已经提供了目录表 tagv_ram 以及数据块 data_ram 的实现，同学们主要需要在已给的 Cache 代码框架（见文件./cache.v）中实现第一级流水的**请求缓存 request_buffer** 以及**控制模块**部分。当然，对于学有余力的同学可以尝试自己实现设计并实现 Cache 主模块、目录表和数据块。

```{image} ../_static/img/labs/cache_pip.png
:width: 90%
:align: center
```

接下来，我们将具体介绍 Cache 两级流水段和控制模块的工作原理和实现，最后以一个简单示例演示 Cache 的工作流程，帮助同学们理解 Cache 各个模块是如何相互协调实现功能的。

#### <br>两级流水段

指令 Cache 支持两级流水段的流水访存。两级流水段为地址流水段和数据流水段，分别与 CPU 的地址握手阶段和数据握手阶段相配合。Cache 在地址流水段从 CPU 接收并存储访存地址，在数据流水段查找并返回数据。

**对于地址流水段**，主要模块是一个请求缓存 request_buffer ，其由多个用于缓存访存地址等信息的寄存器构成。在 CPU 将`cpu_req`拉高且 Cache 未阻塞时，请求缓存会解析出访存地址的 tag、index、offset 等信息。其中，index 和 tag 会被送到下一级流水段中的目录表（图中 Tagv Ram ）中，index 和 offset 会被送到下一级流水段中的数据块中（图中 Data Ram ）中。

请求缓存将这些信息以及访存地址缓存到寄存器中，同时将原本的访存地址缓存在一个额外的寄存器`ll_address`中。其之所以要缓存上上次的访存地址，是因为此时 Cache 刚刚判断出上上次的访存是否命中，若未命中则需要利用`ll_address`中缓存的上上次访存地址向内存中读取数据。

```{note}
关于地址的解析，由于本实验要求的Cache每路为 128 行，因此 index 需要 7 位，即 2^7 = 128；每行由 8 个块构成， 每个块 32 位（4 个字节），因此 offset 需要 5 位，即 2^5 = 8 * 4；地址其余 20 位作为 tag 。
```

```{image} ../_static/img/labs/cache.png
:width: 100%
:align: center
```

<br>**对于数据流水段**，主要有两个模块，分别为目录表和数据块。这两个模块均已给出 verilog 实现示例，同学们可以直接使用或根据自己的设计进行适当修改，也可以尝试自己实现。

*目录表* 用于存储缓存数据内存地址的 tag ，当 CPU 发起访存请求时，Cache 通过到目录表查找访存地址的 tag 来判断缓存是否命中。根据本实验要求，Cache每路共有 128 行，每行一个 tag ，故每路目录表需要维护一个 128 * 20 bit 的 tag 数组，同时，每行还需要额外一位有效位来标记该行是否有缓存数据，故每路目录表需要维护一个 128 * (20+1) bit 大小的数组。此外，目录表还需要支持写入，在 Cache 未命中从内存读取访存数据时，需要对目录表进行更新。以下给出一个使用 Verilog 实现的二路 Cache 的目录表示例供大家参考。

```verilog
/* 该目录表仅缓存了一路的tag，对于两路的设计需要例化两个该目录表 */
/* 该目录表需要一拍时间对访存是否命中进行判断 */
/* 该目录表需要一拍时间进行tag的写入 */
module icache_tagv(
    input          clk,         // 时钟信号
    input          wen,         // 写使能
    input          valid_wdata, // 写入有效位的值，在重启刷新cache时为0，其他情况为1
    input  [6 :0]  index,       // 查找tag或写入时所用的索引
    input  [19:0]  tag,         // CPU访存地址的tag

    output hit                  // 命中结果
    );

    /* --------TagV Ram------- */
    //  |  tag  | valid |
    //  |20    1|0     0|
    reg [20:0] tagv_ram[127:0];

    /* --------Write-------- */
    always @(posedge clk) begin
        if (wen) begin 
            tagv_ram[index] <= {tag, valid_wdata};
        end
    end

    /* --------Read-------- */
    reg [20:0] reg_tagv;
    reg [19:0] reg_tag;
    always @(posedge clk) begin
        reg_tagv = tagv_ram[index];
        reg_tag = tag;
    end
    
    assign hit = (reg_tag == reg_tagv[20:1]) && reg_tagv[0];

endmodule
```
*数据块* 中则真正缓存了数据，其根据 index 查找到对应的 Cache 行，根据 offset 最终确定访问的数据。根据本实验要求，Cache 每路共有 128 行，每行 32 字节，故每路数据块大小为 128 * 32 B 。由于数据块较大，建议使用 Vivado 提供的`block memory generator`自动生成 ram ip核来实现。以实现上述一路数据块为例，生成ip核步骤如下：

```{image} ../_static/img/labs/ip_1.png
:width: 100%
:align: center
```
```{image} ../_static/img/labs/ip_2.png
:width: 90%
:align: center
```
```{image} ../_static/img/labs/ip_3.png
:width: 90%
:align: center
```
```{image} ../_static/img/labs/ip_4.png
:width: 90%
:align: center
```

<br>最后给出一个使用 Verilog 实现的二路 Cache 的数据块示例供大家参考。

```{note}
在使用数据块实例模块时，需要根据上文在实验项目中添加 ram ip 核，但在设置`read width`和`write width`时需要改为 32 而非 256 。
```

```verilog
/* 该数据块仅缓存了一路的数据，对于两路的设计需要例化两个该数据块 */
/* 该数据块的写入和读取数据均需要一拍的时间 */
module icache_data(
    input  clk,              // 时钟信号
    input  [31 :0]  wen,     // 按字节写使能，如 wen = 32'hf000000，则只写入目标行的[31:0]
    input  [6  :0]  index,   // 访存或写入的索引
    input  [4  :0]  offset,  // 访存的偏移量
    input  [255:0]  wdata,   // 写入的数据

    output [31 :0]  rdata    // 访存读出的数据
    );

    // 由于Cache一次读一行，故需要缓存offset在读出一行后利用其确定最终的4字节
    reg [4:0] last_offset;
    always @(posedge clk) begin
        last_offset <= offset;
    end

    //-----调用IP核搭建Cache的数据存储器-----
    wire [31:0] bank_douta [7:0];
    /*
        Cache_Data_RAM: 128行，每行32bit，共8个ram
        接口信号含义：   clka：时钟信号
                        ena: 使能信号，控制整个ip核是否工作
                        wea：按字节写使能信号，每次写4字节，故wea有4位
                        addra：地址信号，说明读/写的地址
                        dina：需要写入的数据，仅在wea == 1时有效
                        douta：读取的数据，在wea == 0时有效，从地址addra处读取出数据
    */
    generate
        genvar i;
        for (i = 0 ; i < 8 ; i = i + 1) begin
            inst_ram BANK(
                .clka(clk),
                .ena(1'b1),
                .wea(wen[i*4+3:i*4]),
                .addra(index),
                .dina(wdata[i*32+31:i*32]),
                .douta(bank_douta[7-i])
            );
        end
    endgenerate

    assign rdata = bank_douta[last_offset[`ICACHE_OFFSET_WIDTH-1:2]];
endmodule
```

```{note}
对于指令 Cache 数据块的写入仅在 Cache 未命中从内存读取数据时才会发生，由于本实验中 AXI 总线单次仅返回 4 字节，故每次对于数据块也只写入 4 字节，这里给出一个简单的示例说明如何使用 icache_data 模块实现特定 4 字节的写入。

该模块涉及写入的接口为 wen、index、wdata，若需要写入的数据`index = 7'h00, offset = 5'h04, data = 32'h12345678`，则对接口赋值`wen = 32'h0f000000, index = 7'h00, wdata = {8{data}}`。其中对于 wen ，由于偏移量为 4 字节，即需要写入的是该行的第二个数据，故将 wen[27:24] 置高，其余置低，以示仅将 wdata 的 [32:63] 写入到目标行的 [32:63] （wen 的每 4 位对应 Cache 行的 4 字节）。
```

当然，两段仅是在 Cache 命中的情况下，若出现未命中的情况，则通过 AXI 总线访存，这是需要耗费多个周期，耗费的周期数与 Cache 自动机在未命中时的状态转移设计以及 AXI 访存机制相关。对于未命中情况的处理，将在`控制模块的实现`中提供实现方案。

#### 控制模块

控制模块主要包含 LRU 模块以及状态自动机模块。

**LRU** 即最近最少使用，LRU 模块用于 Cache 未命中时对缓存块的替换，其只有一个输出`sel_way`，用于告知目录表和数据块需要更换哪一路的数据。

该模块需要维护一个 lru 表，该表记录了最近每行各路的使用情况，在选择替换块时，只需根据该数组找到目标行中最近最少使用的一路进行替换即可。以二路 Cache 为例，该 lru 表只需记录每行上次使用的路的编号即可，则在替换时选择另一路即可，由此可知二路 Cache 的 lru 表每行只需 1 bit，共需 128*1 bit 。此外，lru 表在每次 CPU 访存命中以及未命中进行Cache更新时，都需要进行更新。

**状态自动机模块**用于标识当前 Cache 的工作状态，便于发出相应的控制信号，控制整个 Cache 的运行状态。以下是一个指令 Cache 的 DFA 状态转移示意图，同学们可以将该 DFA 应用到自己的 Cache 中，也可以自行设计 DFA。

```{image} ../_static/img/labs/FSM.png
:width: 90%
:align: center
```

其中各个状态的详细说明如下。

```{list-table}
:header-rows: 1
:widths: 12 88

* - 状态
  - 描述
* - IDLE
  - 空转状态，Cache恢复正常工作前的缓冲状态，会在下一个时钟周期转移到运行状态。
* - RUN
  - 运行状态，Cache不发生数据缺失正常两拍返回数据的状态。发生数据缺失，即目录表输出`hit = 0`时则，会进入选路状态开始进行Cache的替换和更新。
* - SEL_WAY
  - 选路状态，如果Cache发生数据缺失，就会进入这一状态，并根据LRU算法进行选路，为接下来Cache的替换和更新做准备。Cache每次只会在该状态停留一拍用于确定选路，然后自动跳转到缺失状态。
* - MISS
  - 缺失状态，更新 lru 表，并向主存储器发起读数据请求，即给AXI总线发送`arvalid = 1`，同时发送数据地址，请求读取Cache中未命中的行，如果主存储器接收了读请求，即AXI总线返回`arready = 1`，则Cache进入到数据重填状态接收返回的数据。
* - REFILL
  - 数据重填状态，Cache接收主存储器传回的数据，并根据选路状态给出的选路结果更新目录表和数据块，状态持续到数据传输完成，即AXI总线返回`rvalid = 1 && rlast = 1`。
* - FINISH
  - 重填完成状态，标志Cache更新完成，Cache将在下一个时钟周期回到运行状态，重新进行上次未命中的访存。
* - RESETN
  - 初始化状态，当用于处理器初始化时清空cache，持续128个时钟周期，每个周期清空一行。
```

在利用 Verilog 实现时，可以将这7个状态分别映射为数字0-6，并利用一个寄存器`state`记录当前Cache的状态，且每一拍都需要根据给出的自动机规则更新`state`。在实现其他模块时，通常需要先确定当前Cache的状态，此时只需访问`state`。以下给出一个代码框架供同学们参考。

```verilog
parameter idle    = 0;
parameter run     = 1;
parameter sel_way = 2;
parameter miss    = 3;
parameter refill  = 4;
parameter finish  = 5;
parameter resetn  = 6;

reg [2:0] state; 

/* DFA */
always @(posedge clk) begin
    if (!rst) begin
        state <= idle;
    end
    else begin
        /*TODO：根据设计的自动机的状态转移规则进行实现*/
    end
end

/* 某功能模块 */
always @(posedge clk) begin
    if (!rst) begin
        /*TODO：初始化相关寄存器*/
    end
    else begin
        if (state == idle) begin
            /*TODO：该模块在idle状态下的行为*/
        end
        else if (state == run) begin
            /*TODO：该模块在run状态下的行为*/
        end
        ...
    end
end

```

#### 指令 Cache 工作流程

接下来，我们将以一个简单的示例来展示指令 Cache 具体是如何工作的。在这个示例中，CPU 将向 Cache 发送多次访存请求，访存地址依次为`32'hbfc00000, 32'hbfc00004, 32'hbfc00008...`。

开机时，如图 1 ，Cache 需要进行初始化，DFA 的状态处于`RESETN`，Cache 花费 128 个周期将目录表 tagv_ram 中的内容全部初始化为 0。

```{image} ../_static/img/labs/cache_resetn.png
:width: 90%
:align: center
```
<center>图 1</center>

<br>完成初始化后，如图 2 ，DFA 的状态跳转到`IDLE`，说明Cache 目前处于空转状态，等待CPU发送访存请求。当 CPU 发出请求，即`cpu_req = 1`时。此时Cache可以成功接收地址，向 CPU 返回`cache_addr_ok = 1`，使CPU能够继续发送下一个访存请求。

```{image} ../_static/img/labs/cache_idle.png
:width: 90%
:align: center
```
<center>图 2</center>

<br>如图 3 ，在 CPU 把`cpu_req`信号拉高的下一拍（我们可以认为是 Cache 正式开始工作的**第一拍**），DFA 状态跳转到`RUN`，Cache 的地址流水段开始工作，请求缓存将访存请求以及访存地址`32'hbfc00000`进行缓存，其中的`ll_address`用于缓存上上次的访存地址，在 Cache 未命中访问内存时需要用到。同时可以发现，CPU 已经发出了下一个访存请求，访存地址为`32'hbfc00004`。

```{image} ../_static/img/labs/cache_run_1.png
:width: 90%
:align: center
```
<center>图 3</center>

<br>**第二拍**，如图 4 ，请求缓存将访存地址的 tag 和 index 传送给目录表 tagv_ram 判断是否命中，将访存地址的 tag 和 offset 传送给数据块 data_ram 预读取数据。根据图 4 可知此时目录表索引为 5'h04 处的 valid 位为0，说明暂未缓存数据，故未命中，输出 `hit = 0`。此时需要阻塞 Cache 进行内存访问，不可继续接收 CPU 请求，故 Cache 向 CPU 发送信号`cache_addr_ok = 0`。

```{note}
对于阻塞 Cache 的实现，实际只需要使 Cache 的流水段在 Cache 处于`RUN`状态时才正常工作即可，那么在 Cache 未命中跳转到其他状态时，流水段便自然会停止工作，实现阻塞。
```

```{image} ../_static/img/labs/cache_run_2.png
:width: 90%
:align: center
```
<center>图 4</center>

<br>**第三拍**，如图 5 ，由于上一拍未命中，故此时 DFA 状态跳转至`SEL_WAY`。该状态下 LRU 模块开始工作，其根据 LRU 算法选择 Cache 中的某一路用于存储后序从内存中读取的数据。选路工作仅需一拍即可完成，SEL_WAY 状态也只会持续一拍。

```{image} ../_static/img/labs/cache_sel_way.png
:width: 90%
:align: center
```
<center>图 5</center>

<br>**第四拍**，如图 6 ，此时已经完成了选路，DFA 状态跳转至`MISS`，此时LRU将选路结果`sel_way`传送给目录表以及数据块。同时，Cache 将未命中访问的访存地址，即`ll_address`存储的地址，发送给 AXI 总线，并拉高`arvalid`以示向内存发送访存请求，等待至 AXI 总线返回`arready = 1'b1`，此时说明内存已经接收到访存请求和访存地址。此外，还需要对目录表进行更新（当然你也可以在未命中访问内存过程中的其他状态进行更新），如图 6 所示，LRU 模块选择了第 0 路，故目录表中索引为0的第0路的表项被更新为`bfc00`，且有效位被置1。

```{image} ../_static/img/labs/cache_miss.png
:width: 90%
:align: center
```
<center>图 6</center>

<br>如图 7 ，当AXI总线返回`arready = 1'b1`时（**注意 Cache 拉高`arvalid`信号后，AXI总线并不一定在下一拍立刻返回`arready = 1'b1`，因为AXI总线可能会被其他事务占用，导致 Cache 向AXI总线发送的请求可能会延迟几个时钟周期再得到响应**），DFA状态跳转至`REFILL`，Cache 拉高`rready`以示可以接收内存向 AXI 总线发送的数据。本实验中，AXI 总线一次仅能发送4字节，而 Cache 一行共32字节，故需要接收 8 个 AXI 总线发送的数据，至少需要占用 8 个时钟周期。每次 AXI 总线发送数据，均会将`rvalid`信号拉高，而当`rlast`信号被拉高时，说明这是此次传输任务中 AXI 总线发送的最后一个数据。Cache 的工作就是在`rvalid = 1`时，将传输的数据写入数据块对应项的对应位置中。

```{image} ../_static/img/labs/cache_refill_1.png
:width: 90%
:align: center
```
<center>图 7</center>

<br>当 Cache 收到`rlast = 1`，即AXI总线返回最后一条指令时，说明数据传输即将完成。

```{image} ../_static/img/labs/cache_refill_2.png
:width: 90%
:align: center
```
<center>图 8</center>

<br>如图 9 ，数据传输完成后，即 Cache 收到`rlast = 1`的下一拍，DFA 状态跳转至`FINISH`，Cache 会根据上次未命中的访存地址再次查找，显然这次必然会命中，查找结果会在下一拍输出。

```{image} ../_static/img/labs/cache_finish.png
:width: 90%
:align: center
```
<center>图 9</center>

<br>接下来一拍，如图 10 ，由于地址流水段还缓存有访存请求，故DFA状态跳转到`RUN`。此时，Cache 已经输出内存地址为`32'hbfc00000`的指令，同时 Cache 拉高`cache_data_ok`以示 CPU 接收当前返回的指令。此外，Cache 已解除阻塞，可以正常进行流水工作，故信号`cache_addr_ok`拉高继续接收 CPU 访存请求。

```{image} ../_static/img/labs/cache_run_3.png
:width: 90%
:align: center
```
<center>图 10</center>

<br>在成功返回第一个访存请求的结果的下一拍，如图 11 ，Cache 中缓存的第二个访存请求也进入到了数据流水段。由于第二个访存地址为`32'hbfc00004`，与第一个访存地址`32'hbfc00000`相邻且在同一个 Cache 行，故这次访问显然是会命中的，于是 DFA 状态仍然为`RUN`，这一拍 Cache 也能够返回一条指令，信号`cache_data_ok`继续拉高使 CPU 读取返回的指令。

与此同时，地址流水段的请求缓存也进行了刷新，读入了第三次访存请求的内容。由于一次未命中后，Cache会向内存一次性读取一行，即 8 条指令，故在访问连续存储的指令的情况下，每次未命中后，Cache 会连续命中 7 次，即接下来的 7 拍均会返回有效指令，实现流水化工作。

```{image} ../_static/img/labs/cache_run_4.png
:width: 90%
:align: center
```
<center>图 11</center>

<br>以上即是一个简单指令 Cache 在未命中和命中情况下的大致工作过程。

### 实现类 Sram 协议

给出本次实验中，同学们设计的指令 Cache 与 CPU 连接的接口：

```verilog
module cache (
    input            clk             ,  // clock, 100MHz
    input            rst             ,  // active low

    //  Sram-Like接口信号定义:
    //  1. cpu_req     标识CPU向Cache发起访存请求的信号，当CPU需要从Cache读取数据时，该信号置为1
    //  2. cpu_addr    CPU需要读取的数据在存储器中的地址,即访存地址
    //  3. cache_rdata 从Cache中读取的数据，由Cache向CPU返回
    //  4. addr_ok     标识Cache和CPU地址握手成功的信号，值为1表明Cache成功接收CPU发送的地址
    //  5. data_ok     标识Cache和CPU完成数据传送的信号，值为1表明CPU在本时钟周期内完成数据接收
    input         cpu_req      ,    //由CPU发送至Cache
    input  [31:0] cpu_addr     ,    //由CPU发送至Cache
    output [31:0] cache_rdata  ,    //由Cache返回给CPU
    output        cache_addr_ok,    //由Cache返回给CPU
    output        cache_data_ok,    //由Cache返回给CPU

    ...
);

    /*TODO：完成指令Cache的设计代码*/

endmodule
```

接下来通过指令 Cache 与 CPU 在工作中的交互过程，具体解释以上给出的各个接口的具体作用。

通常指令 Cache 在流水线中占用两拍（ cache 命中的情况下），如下图所示（`cpu_`前缀的信号由 CPU 发给 Cache ，`cache_`前缀的信号由 cache 发给 CPU ）。

```{image} ../_static/img/labs/cache_cpu协议.png
:width: 90%
:align: center
```

<br>第一拍：CPU 在这一拍之前拉高读请求信号`cpu_req`并给出读地址`cpu_addr`。Cache拉高`cache_addr_ok`表示 Cache 已经接收读地址，Cache 和 CPU 地址握手成功。

第二拍：若 Cache 命中，则 Cache 在第二拍返回给 CPU 读出的数据/指令`cache_rdata`，同时拉高`cache_data_ok`使 CPU 读取指令，至此 Cache 和 CPU 完成数据传送。注意，返回一条指令则`cache_data_ok`只能拉高一拍，该信号拉高 X 拍表示返回 X 条指令。

### 实现 AXI 协议

给出本次实验中，同学们设计的指令 Cache 与 AXI 总线连接的接口：

```verilog
module cache (
    input            clk             ,  // clock, 100MHz
    input            rst             ,  // active low

    //  Sram-Like接口信号定义:
    ...

    //  AXI接口信号定义:
    //  Cache与AXI的数据交换分为两个阶段：地址握手阶段和数据握手阶段
    output [3 :0] arid   ,              //Cache向主存发起读请求时使用的AXI信道的id号，设置为0即可
    output [31:0] araddr ,              //Cache向主存发起读请求时所使用的地址
    output        arvalid,              //Cache向主存发起读请求的请求信号
    input         arready,              //读请求能否被接收的握手信号

    input  [3 :0] rid    ,              //主存向Cache返回数据时使用的AXI信道的id号，设置为0即可
    input  [31:0] rdata  ,              //主存向Cache返回的数据
    input         rlast  ,              //是否是主存向Cache返回的最后一个数据
    input         rvalid ,              //主存向Cache返回数据时的数据有效信号
    output        rready                //标识当前的Cache已经准备好可以接收主存返回的数据
);

    /*TODO：完成指令Cache的设计代码*/

endmodule
```

指令 Cache 与 AXI 的数据交换分为两个阶段：地址握手阶段和数据握手阶段。

在地址握手阶段，如下图所示，Cache 给出使用的 AXI 信道的 id 号和目标地址，同时拉高`arvalid`以示发起读请求。`arready`拉高时说明此时总线可以接收读请求。当`arvalid`和`arready`同时拉高则说明地址握手阶段完成。

```{image} ../_static/img/labs/AXI_addr.png
:width: 90%
:align: center
```

<br>在数据握手阶段，如下图所示，主存给出使用的 AXI 信道的 id 号和目标数据，同时拉高`rvalid`以示返回数据有效，请求方（即 Cache ）可以读取该数据。当主存返回最后 4 字节时，会拉高`rlast`以示这是最后一次数据传输。`rready`拉高时说明 Cache 可以接收主存返回的数据。

```{image} ../_static/img/labs/AXI_data.png
:width: 90%
:align: center
```

<br>本次实验只涉及 AXI 协议的部分内容，感兴趣想要详细了解 AXI 协议的同学可以下载[AMBA® AXI Protocol](https://developer.arm.com/documentation/ihi0022/b/)自行阅读。

## 实验要求

根据实验内容中的描述实现一个指令 Cache 。对于目录表和数据块的实现，可以采用指导书的参考示例，也可以自行实现；对于 Cache 主模块的实现，可以使用实验环境中提供的代码框架（即`./cache.v`文件，该文件未添加到 Vivado 项目中），也可以完全自行设计实现 Cache 主模块，但是必须确保 Cache 对外暴露的端口正确（端口要求见`8.5.1 使用要求`）。

**Verilog 语言实现**：要求采用结构化设计方法，用 Verilog 语言实现处理器的设计。设计包括：

  * 各模块的详细设计（包括各模块功能详述，设计方法，Verilog 语言实现等）
  * 各模块的功能测试（每个模块作为一个部分，包括测试方案、测试过程和测 试波形等）
  * 系统的详细设计（包括系统功能详述，设计方法，Verilog 语言实现等）
  * 系统的功能测试（包括系统整体功能的测试方案、测试过程和测试波形等）

对 Cache 进行仿真测试，记录运行过程和结果，完成实验报告。

## 测试环境

为了帮助各位同学更好地完成实验，实验四要求使用我们提供的 Cache 测试环境对 Cache 进行仿真测试。

### 使用要求

要求每个同学的 Cache 对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module cache (
    input            clk             ,  // clock, 100MHz
    input            rst             ,  // active low

    //  Sram-Like接口信号定义:
    input         cpu_req      ,    //由CPU发送至Cache
    input  [31:0] cpu_addr     ,    //由CPU发送至Cache
    output [31:0] cache_rdata  ,    //由Cache返回给CPU
    output        cache_addr_ok,    //由Cache返回给CPU
    output        cache_data_ok,    //由Cache返回给CPU

    //  AXI接口信号定义:
    output [3 :0] arid   ,              //Cache向主存发起读请求时使用的AXI信道的id号
    output [31:0] araddr ,              //Cache向主存发起读请求时所使用的地址
    output        arvalid,              //Cache向主存发起读请求的请求信号
    input         arready,              //读请求能否被接收的握手信号

    input  [3 :0] rid    ,              //主存向Cache返回数据时使用的AXI信道的id号
    input  [31:0] rdata  ,              //主存向Cache返回的数据
    input         rlast  ,              //是否是主存向Cache返回的最后一个数据
    input         rvalid ,              //主存向Cache返回数据时的数据有效信号
    output        rready                //标识当前的Cache已经准备好可以接收主存返回的数据
);

    /*TODO：完成指令Cache的设计代码*/

endmodule
```

```{warning}
* 只允许按照给定的接口格式去设计 Cache ，不允许更改接口格式
* 所有信号在时钟上升沿采样
* 复位后，尽量保证上述接口信号不出现 X 或 Z
* 仅需添加待完成的 Cache ，其他部分不要修改
```

### 仿真结果

当所实现的 Cache 功能正确时，会在控制台打印**PASS**，如下图所示。

```{image} ../_static/img/labs/cache_success.png
:width: 90%
:align: center
```

<br>当所实现的 Cache 出现错误时，会在控制台打印错误信息，如下图所示。

其中 Reference 一行打印的是正确的CPU的输出信息，Error 一行打印的是你所实现的 CPU 的输出信息。

```{image} ../_static/img/labs/cache_error.png
:width: 90%
:align: center
```

