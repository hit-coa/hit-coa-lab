# 附加实验 4 :  LoongArch指令集处理器设计

## 实验概述

基于《CPU设计实战：LoongArch版》完成LoongArch架构下的cpu设计
书籍链接：https://bookdown.org/loongson/_book3/
代码仓库：https://gitee.com/loongson-edu/cdp_ede_local

## 实验内容

完成前8章内容。（至实践任务16：完成AXI随机延迟验证）