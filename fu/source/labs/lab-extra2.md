# 附加实验 2 ：数据 Cache 的设计与实现

附加实验要求在实验4实现的指令 Cache 上添加写功能，从而将指令 Cache 扩展为数据 Cache。

## 实验概述

实现一个采用写回策略的数据 Cache 。对于 lab 3 的指令 Cache ，我们只实现了 Cache 从内存读取数据的功能。相比之下，数据 Cache 还需要实现修改缓存的数据、将修改过的缓存数据写回内存（对于缓存中从未被修改的数据，被替换时就没有必要写回内存）的功能。

一个数据 Cache 应当对外暴露以下接口。

```verilog
module cache(
    input         clk,
    input         rst,

    input         cpu_req      , // CPU 请求
    input         cpu_wr       , // 0-读请求；1-写请求
    input  [3 :0] cpu_wstrb    , // 按字节写使能
    input  [31:0] cpu_addr     , // 访存地址
    input  [31:0] cpu_wdata    , // 写入的数据
    output [31:0] cache_rdata  , // 读请求结果
    output        cache_addr_ok, // 值为1表明Cache成功接收CPU发送的地址
    output        cache_data_ok, // 值为1表明Cache成功返回CPU读请求的数据或成功写入CPU写请求的数据

    // AXI接口信号
    // Read
    output [3 :0] arid    , // 置 0 即可
    output [31:0] araddr  , // 地址
    output [7 :0] arlen   , // 传输次数
    output [2 :0] arsize  , // 单次传输数据的大小
    output [1 :0] arburst , // 置 0 即可
    output [1 :0] arlock  , // 置 0 即可
    output [3 :0] arcache , // 置 0 即可
    output [2 :0] arprot  , // 置 0 即可
    output        arvalid , // 发起读请求
    input         arready ,

    input  [3 :0] rid     , 
    input  [1 :0] rresp   ,
    input  [31:0] rdata   ,
    input         rlast   ,
    input         rvalid  ,
    output        rready  ,

    // Write
    output [3 :0] awid    , // 置 0 即可
    output [31:0] awaddr  , // 地址
    output [7 :0] awlen   , // 传输次数
    output [2 :0] awsize  , // 单次传输数据的大小
    output [1 :0] awburst , // 置 0 即可
    output [1 :0] awlock  , // 置 0 即可
    output [3 :0] awcache , // 置 0 即可
    output [2 :0] awprot  , // 置 0 即可
    output        awvalid , // 发起写请求
    input         awready ,

    output [3 :0] wid     , // 置 0 即可
    output [31:0] wdata   , // 写入的数据
    output [3 :0] wstrb   , // 按字节写使能
    output        wlast   , // 值为1说明这是写回的最后一次传输
    output        wvalid  , // 本次数据有效
    input         wready  ,

    input  [3 :0] bid     ,
    input  [1 :0] bresp   ,
    input         bvalid  ,
    output        bready  
);

endmodule
```

## 实现思路

### 修改缓存的数据

由于该 Cache 采用“写回”的策略，故当 CPU 发起写请求且缓存命中时，Cache 只需简单地修改请求地址对应的缓存数据即可，待以后的某个时刻该缓存块需要被替换时，才将其写回内存从而实现内存的更新。而当写不命中时，需要先从内存读出请求块，再在缓存中对该请求块进行修改，同样不需要再写回内存，待后序被替换即可。

### 将“脏块”写回内存

首先，为了辨别脏块，对于 Cache 缓存的每一行，我们需要一位来标记该行是否被 CPU 改写过，即为每一行添加一个“脏位”。

其次，由于该 Cache 采用“写回”的策略，“脏块”的写回只会发生在 Cache 不命中的时候。此时，我们有两个选择：
* 一是先将“脏块”写回内存，待写回完成后，再向内存读取请求块；
* 二是向 AXI 总线发出写请求后，立刻再发出读请求。
 
显然第一种选择虽简单，但会导致 Cache 不命中的开销极大，且其并没有充分利用 AXI 总线同时支持读写请求的优势，故第二种选择才是更好的方法，这也决定了 Cache 对写回内存的处理和读取内存的处理必须是并行的，但写回内存的开销可能比读取内存更大，即 Cache 已经读取了目标块完成了当前的 CPU 请求并继续执行写一个请求，但 Cache 对于“脏块”的写回仍然未完成，故实际上 Cache 对写回内存的处理应当与 Cache 的其他所有工作并行。

从实现角度讲，一方面我们需要一组寄存器来缓存待写回的数据，另一方面我们需要一个额外独立的状态自动机来控制 Cache 的写回。对于该状态自动机，只需要`IDLE`和`WRITE_BACK`两个状态（当然同学们在实现时不一定局限于仅用两个状态，可以自行设计自动机），在 Cache 不需要写回“脏块”的时候，该状态机始终处于`IDLE`状态，当 Cache 发生不命中时，该状态机状态跳转为`WRITE_BACK`状态，并在完成写回后跳转回`IDLE`状态。

此外，由于写回内存并不一定能在本次 CPU 请求中完成，故存在后续 CPU 再次访问写回的数据但写回仍未完成的情况，对此我们可以选择暂时阻塞 Cache 不接收 CPU 的请求，待写回完成后再继续处理 CPU 的请求。

### 补充：AXI 协议

相比指令 Cache ，我们需要通过 AXI 总线将数据写回内存，这里需要用到 AXI 总线的写通道。对于 AXI 协议的读写相关信号在`10.1 实验概述`中给出的 Cache 的接口中可以找到。这里简单说明 Cache 通过 AXI 总线写回内存的过程：

* 首先，Cache 向总线发起写请求`awvalid = 1`，并给出写的起始地址`awaddr`、每次写数据的大小`awsize`以及写的次数`awlen`；
* 待 AXI 总线返回`awready = 1`，则说明此时写请求已经被接收；
* 接下来，Cache 即可传输数据，每次传输数据，Cache 发送`wvalid = 1`以示该数据有效；
* 待 AXI 总线给出`wready`以示其接受该数据后，Cache重复上一步骤进行下一次传输；
* 当 Cache 在发送最后一个数据时，需要额外发送`wlast = 1`以示这是最后一次数据传输。

以上只是对Cache 通过 AXI 总线写回内存的过程的大致描述，为确保同学们准确地了解具体各个信号的含义及如何使用使用，请下载[AXI 协议手册](https://developer.arm.com/documentation/ihi0022/b/)，查看`Chapter 2. Signal Descriptions`章节。

## 参考资料

1. [AXI 协议手册](https://developer.arm.com/documentation/ihi0022/b/)