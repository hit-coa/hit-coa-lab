# Vivado安装说明

在学习并尝试本章节前，你需要具有以下环境：
* Windows 或 Linux 操作系统的电脑一台。
* 连通的网络。
  
通过本章节的学习，你将获得：
* Vivado 的下载方法。
* Vivado 的在线安装方法。

本地安装与在线安装的优劣势对比如下。
| 安装方法 | 优势 | 劣势 |
|---------|------|------|
| 本地安装 |1. 安装过程不需要联网。|1. 本地安装包有 26G 。提前下载或拷贝较慢。<br>2. 下载过程中需要注册 Xilinx 账号，以后会不停收到一堆推荐邮件。|
| 在线安装 | 1. 安装包小，只有 50M 左右。<br>2. 安装可根据需求选择版本和支持的器件。|1. 安装过程中需要联网，且不能断网。<br>2. 下载安装包和在线安装时，需要 Xilinx账号。|


## Vivado简介

Vivado 是 Xilinx 的一款开发套件 EDA 工具。下面以Vivado 2019.2 的 WebPACK 版本在 Windows 上安装为例，WebPACK 版本为免 license 的 Vivado 免费版，支持的器件受限，该版本支持 Artix-7 器件的开发，足够完成本实验，默认安装要求至少 16GB 的硬盘空间。Vivado 2019.2 支持以下操作系统：
- Windows 7.1: 64-bit
- Windows 10 Professional versions 1809 and 1903: 64-bit
- Red Hat Enterprise Linux 7.4-7.6: 64-bit
- CentOS Linux 7.4-7.6: 64-bit
- SUSE Enterprise Linux 12.4: 64-bit
- Amazon Linux 2 AL2 LTS: 64-bit
- Ubuntu Linux 16.04.5, 16.04.6, 18.04.1 and 18.04.2 LTS: 64-bit 
- Additional library installation required 

其他不同版本的安装和使用具体参考相应版本的用户指导手册。

## Vivado 安装文件的下载

在 [Xilinx官网](https://china.xilinx.com/support/download.html) 下载所需的 Vivado 版本。可以选择先下载安装 Web Installer，再通过安装器下载安装，可以减小下载时间和下载安装包大小；也可以直接下载安装包文件来安装，文件大小约26GB。

```{image} ../_static/img/tutorial/Vivado_Design_Suite_download.png
:alt: Vivado_Design_Suite_download
:width: 100%
:align: center
```

<br>Vivado 设计套件提供支持 windows 系统、Linux 系统的在线安装器和支持双系统的本地安装包下载，选择相应
的版本下载。这里选择 Windows 环境下的在线安装器（Windows Self Extracting Web Installer）。

```{image} ../_static/img/tutorial/Vivado_2019.2_download.png
:alt: Vivado_2019.2_download
:width: 70%
:align: center
```

<br>下载需要登陆 Xilinx。如果已有 Xilinx 账户直接填写用户名和密码登陆，如果没有账户则点“Create account”免费创建一个新账户。

```{image} ../_static/img/tutorial/Xilinx_load.png
:alt: Xilinx_load
:width: 45%
:align: center
```

<br>进行姓名和地址验证。输入信息后点网页底部的“下载”。

```{image} ../_static/img/tutorial/name_address_check.png
:alt: name_address_check
:width: 100%
:align: center
```

<br>此时弹出保存 Vivado 安装包的窗口，将安装包保存在合适的地方。

## Vivado 在线安装

下载后，双击运行已下载的可执行文件 Xilinx_Unified_2019.2_1106_2127_Win64.exe。

欢迎界面，点击“Next”。

```{image} ../_static/img/tutorial/vivado_install.png
:alt: vivado_install
:width: 100%
:align: center
```

<br>输入 Xilinx 账户、密码，选择“Download and Install Now”，点击“Next”。

```{image} ../_static/img/tutorial/input_account.png
:alt: input_account
:width: 100%
:align: center
```

<br>勾选所有“I Agree”选项，点击“Next”。

```{image} ../_static/img/tutorial/accept_license_agreement.png
:alt: accept_license_agreement
:width: 100%
:align: center
```

<br>选择 Xilinx 的产品，这里勾选 Vivado，点击“Next”。

```{image} ../_static/img/tutorial/select_product.png
:alt: select_product
:width: 100%
:align: center
```

<br>选择 Vivado 安装版本，这里勾选 Vivado HL WebPACK 版本，点击“Next”。

```{image} ../_static/img/tutorial/select_edition.png
:alt: select_edition
:width: 100%
:align: center
```

<br>选择设计工具、支持的器件。“Design Tools”默认已选择“Design Vivado Suite”和“DocNav”；“Device”默认选择“Artix-7”，正好开发板搭载的 FPGA 是 Artix-7，其他器件可以根据需要进行选择；“Installation Options”按照默认即可。点击“Next”。

```{image} ../_static/img/tutorial/hl_webpack.png
:alt: hl_webpack
:width: 100%
:align: center
```

<br>选择 Vivado 安装目录，默认安装在“C:\Xilinx”下，可以点击浏览或者直接更改路径。点击“Next”。

```{note}
安装路径中不能出现中文和空格。
```

```{image} ../_static/img/tutorial/select_directory.png
:alt: select_directory
:width: 100%
:align: center
```

<br>确认无误，点击“Install”开始安装；如果要修改安装设置，点击“Back”返回到相应的界面修改。

```{image} ../_static/img/tutorial/installation_summary.png
:alt: installation_summary
:width: 100%
:align: center
```

<br>安装进度窗口，等待安装完成。

```{image} ../_static/img/tutorial/installation_progress.png
:alt: installation_progress
:width: 100%
:align: center
```

<br>安装成功后会出提示窗口，点击“确定”。

```{image} ../_static/img/tutorial/installation_finish.png
:alt: installation_finish
:width: 100%
:align: center
```

## 辅助工具

我们推荐使用以下工具：

* Visual Studio Code，现代化的代码编辑工具
* VS Code插件: [Verilog-HDL](https://marketplace.visualstudio.com/items?itemName=mshr-h.VerilogHDL)
* [Icarus Verilog](http://iverilog.icarus.com/)或者Verilator on WSL，除了仿真外，二者都可以配合插件进行语法检查
* Ctags, 配合插件可以一键例化，减少工作量

这些工具能够帮助同学们更加高效地使用vivado，同学们可以选择性地进行安装。当然，仅使用vivado足以完成实验的所有内容。