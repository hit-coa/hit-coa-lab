# Vivado 使用说明

通过本章节的学习，你将获得：
* 初步熟悉龙芯 Artix-7 实验箱。
* 基本的 Vivado 使用方法。
* 初步了解 Vivado 的 XDC 约束文件。

Vivado 有两种开发模式，Project Mode 和 Non-Project Mode。两者的主要区别是，Project Mode 使用 Flow Navigation 更加自动化，Non-Project Mode 使用 Tcl 命令直接控制操作，更手动但更不受限制。实际中根据开发需求选择这两种模式，一般简单设计中采用 Project Mode。

下面以一个简单的流水灯实验为例介绍如何使用 Vivado 新建一个工程。

## FPGA设计流程简介

FPGA即现场可编程门阵列，不过FPGA内部最小单元其实并不是一个个与或非门，而是一些更高层的模块（查找表、触发器、进位链等）。

Vivado的作用是，将我们的Verilog源码映射到FPGA芯片上，用一种类似搭积木的方式实现我们的设计。

和编译一个C程序需要编译链接一样，编译一个Verilog设计也需要若干步骤：

* 第一步： 综合(Synthesize)，将HDL语言、原理图等设计输入翻译成由与、或、非门和RAM、触发器等基本逻辑单元的逻辑连接（网表），并根据目标和要求（约束条件）优化所生成的逻辑连接。
* 第二步：实现（Implementation）。将综合输出的逻辑网表翻译成所选器件的底层模块与硬件原语，将设计映射到器件结构上，进行布局布线，达到在选定器件上实现设计的目的。

其中实现又可分为3个步骤：翻译（Translate）逻辑网表，映射（Map）到器件单元与布局布线（Place & Route）。
* 翻译：将综合输出的逻辑网表翻译为 Xilinx特定器件的底层结构和硬件原语
* 映射：将设计映射到具体型号的器件上（LUT、 FF、 Carry 等）。
* 布局布线：根据用户约束和物理约束，对设计模块进行实际的布局，并根据设计连接，对布局后的模块进行布线，产生比特流文件。

目前多数FPGA的逻辑块都成二维阵列状排列，因此逻辑块布局问题可以被视为二次分配问题，是一种NP问题，因此只能使用模拟退火算法等方法得到符合要求的近似解（就算如此耗时也很长）。这就是为什么我们将管脚分配、时钟定义等称为约束。

## 使用Vivado进行FPGA设计流程

### 新建工程

打开Vivado 2019.2，在界面上“Quick Start”下选择“Create Project”。

```{image} ../_static/img/tutorial/create_project.png
:alt: create_project
:width: 100%
:align: center
```

<br>新建工程向导，点击“Next”。

```{image} ../_static/img/tutorial/new_project_0.png
:alt: new_project_0
:width: 100%
:align: center
```

<br>输入工程名称并选择工程的文件位置，并勾选“Create project subdirectory”选项，为工程在指定存储路径下建立独立的文件夹。设置完成后，点击“Next”。
  
```{note}
工程名称和存储路径中不能出现中文和空格，建议工程名称以字母、数字、下划线来组成。
```

```{image} ../_static/img/tutorial/project_name.png
:alt: project_name
:width: 100%
:align: center
```

<br>选择“RTL Project”一项，并勾选“Do not specify sources at this time”，勾选该选项是为了跳过在新建工程的过程中添加设计源文件，如果要在新建工程时添加源文件则不勾选。点击“Next”。

```{image} ../_static/img/tutorial/project_type.png
:alt: project_type
:width: 100%
:align: center
```

<br>根据使用的 FPGA 开发平台，选择对应的 FPGA 目标器件。根据实验平台搭载的 FPGA，在筛选器的“Family”选择`Artix 7`，“Package”选择`fbg676`，在筛选得到的型号里面选择`xc7a200tfbg676-2`。点击“Next”。

```{image} ../_static/img/tutorial/default_part.png
:alt: default_part
:width: 100%
:align: center
```

<br>确认工程设置信息是否正确。正确点击“Finish”，不正确则点击“上一步”返回相应步骤修改。

```{image} ../_static/img/tutorial/new_project_summary.png
:alt: new_project_summary
:width: 100%
:align: center
```

<br>完成工程新建。

```{image} ../_static/img/tutorial/new_project_finish.png
:alt: new_project_finish
:width: 100%
:align: center
```

### RTL 设计输入

以使用 Verilog 完成 RTL 设计为例。Verilog 代码都是以“.v”为后缀名的文件，可以在其他文件编辑器里写好，再添加到新建的工程中，也可以在工程中新建一个再编辑。

<br>添加源文件。在“Flow Navigator”窗口下的“Project Manager”下点击“Add sources”，或者点击“Source”窗口下“Add Sources”按钮，或者使用快捷键“Alt + A”。

```{image} ../_static/img/tutorial/add_source.png
:alt: add_source
:width: 70%
:align: center
```

<br>添加设计文件。选择“Add or create design sources”来添加或新建 Verilog 或 VHDL 源文件，点击“Next”。

```{image} ../_static/img/tutorial/source_type.png
:alt: source_type
:width: 100%
:align: center
```

<br>添加或者新建设计文件。如添加已有设计文件或者添加包含已有设计文件的文件夹，选择“Add Files”或者“Add Directories”，然后在文件浏览窗口选择已有的设计文件完成添加。如创建新的设计文件，则选择“Create File”。这里新建文件。

```{image} ../_static/img/tutorial/add_or_create_files.png
:alt: add_or_create_files
:width: 100%
:align: center
```

<br>设置新创建文件的类型、名称和文件位置。注意：文件名称和位置路径中不能出现中文和空格。

```{image} ../_static/img/tutorial/name_file.png
:alt: name_file
:width: 70%
:align: center
```

<br>继续添加设计文件或者修改已添加设计文件设置。点击“Finish”。

```{image} ../_static/img/tutorial/add_or_create_files_finish.png
:alt: add_or_create_files_finish
:width: 100%
:align: center
```

<br>模块端口设置。在“Module Definition”中的“I/O Port Definitions”，输入设计模块所需的端口，并设置端口方向，如果端口为总线型，勾选“Bus”选项，并通过“MSB”和“LSB”确定总线宽度。完成后点击“OK”。端口设置也可以在编辑源文件时完成，即可以在这一步直接点“OK”跳过。

```{image} ../_static/img/tutorial/design_port.png
:alt: design_port
:width: 100%
:align: center
```

<br>双击“Sources”中的“Design Sources”下的“blink.v”中打开该文件，输入相应的设计代码。如果设置时文件位置按默认的<Local to Project>，则设计文件位于工程目录下的“\blink.srcs\sources_1\new”中。完成的设计文件如下图所示。

```{image} ../_static/img/tutorial/add_source_finish.png
:alt: add_source_finish
:width: 100%
:align: center
```

以下给出blink.v示例文件。

```verilog
// blink.v
`timescale 1ns / 1ps

module blink (
    input       clk  ,
    input       reset,

    output [7:0] leds
);

    reg [7 :0] leds_reg;
    reg [26:0] cnt;

    assign leds = leds_reg;

    // clock freq = 100MHz
    always @(posedge clk) begin
        if (!reset) begin
            leds_reg <= 8'b11111110;
            cnt      <= 27'd0;
        end else if (cnt == 27'd100000000) begin
            cnt      <= 27'd0;
            leds_reg <= {leds_reg[6:0], leds_reg[7]};
        end else begin
            cnt      <= cnt + 27'd1;
        end
    end

endmodule
```

### 功能仿真

Vivado 集成了仿真器 Vivado Simulator，这里介绍使用 Vivado 仿真器仿真的方法。也可以使用 ModelSim 进行仿真。

首先添加测试激励文件。该步骤与`3.2.2 RTL 设计输入`中的第一步添加源文件类似。

```{image} ../_static/img/tutorial/add_source.png
:alt: add_source
:width: 70%
:align: center
```

<br>在“Add Source”界面中选择“Add or Create Simulation Sources”，点击“Next”。

```{image} ../_static/img/tutorial/source_type_sim.png
:alt: source_type_sim
:width: 100%
:align: center
```

<br>选择“Create File”，新建一个激励测试文件。

```{image} ../_static/img/tutorial/source_type_sim.png
:alt: source_type_sim
:width: 100%
:align: center
```

<br>输入激励测试文件名，点击“OK”。

```{image} ../_static/img/tutorial/name_file_sim.png
:alt: name_file_sim
:width: 70%
:align: center
```

<br>完成新建测试文件，点击“Finish”。

```{image} ../_static/img/tutorial/add_or_create_files_finish_sim.png
:alt: add_or_create_files_finish_sim
:width: 100%
:align: center
```

<br>对测试激励文件进行 module 端口定义，由于激励测试文件不需要有对外的接口，所以不进行 I/O 端口设置直接点击“OK”，完成空白的激励测试文件创建。

```{image} ../_static/img/tutorial/design_port_sim.png
:alt: design_port_sim
:width: 100%
:align: center
```

<br>在“Source”窗口下双击打开空白的激励测试文件。测试文件 blink_tb.v 位于工程目录下“\blink.srcs\sim_1\new”文件夹下。

```{image} ../_static/img/tutorial/add_source_finish_sim.png
:alt: add_source_finish_sim
:width: 70%
:align: center
```

<br>完成对将要仿真的 module 的实例化和激励代码的编写，如下述代码所示。

```verilog
// blink_tb.v
`timescale 1ns / 1ps

module blink_tb;

    reg clk, reset;
    wire [7:0] leds;

    initial begin
        clk = 0;
        reset = 0;
    #50 reset = 1;
    end

    always #5 clk = ~clk;

    blink u_blink(
        .clk   (clk   ),
        .reset (reset ),
        .leds  (leds  )
    );

endmodule
```

<br>进入仿真。在左侧“Flow Navigator”窗口中点击“Simulation”下的“Run Simulation”选项，选择“Run Behavioral Simulation”，进入仿真界面。

```{image} ../_static/img/tutorial/run_sim.png
:alt: run_sim
:width: 100%
:align: center
```

```{image} ../_static/img/tutorial/behave_sim_window.png
:alt: behave_sim_window
:width: 100%
:align: center
```

<br>可通过左侧“Scope”一栏中的目录结构定位到想要查看的 module 内部信号，在“Objects”对应的信号名称上右击选择“Add To Wave Window”，将信号加入波形图中，也可以利用鼠标将信号直接拖拽入 Wave Window 中。仿真器默认显示 I/O 信号，由于这个示例不存在内部信号，因此不需要添加观察信号。

```{image} ../_static/img/tutorial/add_to_wave_window.png
:alt: add_to_wave_window
:width: 100%
:align: center
```

<br>可通过选择工具栏中的选项来进行波形的仿真时间控制。如下图工具条，分别是复位波形（即清空现有波形）、运行仿真、运行特定时长的仿真、仿真时长设置、仿真时长单位、 单步运行、暂停、重启动。

```{image} ../_static/img/tutorial/sim_control_tool.png
:alt: sim_control_tool
:width: 70%
:align: center
```

<br>观察仿真波形是否符合预期功能。在波形显示窗口上侧是波形图控制工具，由左到右分别是：查找、保存波形配置、放大、缩小、缩放到全显示、缩放到光标、转到时间 0、转到时间的最后、前一个跳变、下一次跳变、添加标记、前标记、下一个标记、交换光标。

```{image} ../_static/img/tutorial/wave_control_tool.png
:alt: wave_control_tool
:width: 70%
:align: center
```

<br>可通过右键选中信号来改变信号的显示形态。如下图将信号改为二进制显示。

```{image} ../_static/img/tutorial/radix_control.png
:alt: radix_control
:width: 100%
:align: center
```

### 添加约束文件

添加约束文件有两种方法，一是利用 Vivado 中 I/O Planning 功能，二是直接新建 XDC 的约束文件，手动输入约束命令。下面分别介绍这两种方法。

#### 利用 I/O Planning 生成约束文件

点击“Flow Navigator”中“Synthesis”下的“Run Synthesis”，在弹出的“Launch Runs”窗口点“OK”，先对工程进行综合。

```{image} ../_static/img/tutorial/run_syn.png
:alt: run_syn
:width: 70%
:align: center
```

<br>综合完成之后，选择“Open Synthesized Design”，打开综合后的网表。

```{image} ../_static/img/tutorial/syn_complete.png
:alt: syn_complete
:width: 70%
:align: center
```

<br>此时应看到如下界面，如果没出现如下界面，在图示位置的“layout”中选择“I/O Planning”一项。

```{image} ../_static/img/tutorial/IO_planning.png
:alt: IO_planning
:width: 100%
:align: center
```

<br>在界面右下方的选项卡中切换到“I/O Ports”一栏，并在对应的信号后，在“Site”一列输入对应的 FPGA 管脚标号（或将信号拖拽到右上方“Package”图中对应的管脚上），并指定“I/O std”。具体的 FPGA 约束管脚和I/O 电平标准，可参考对应板卡的原理图或提供的常用的引脚对应关系表（参见龙芯 Artix-7 实验箱-原理图与引脚列表）。

```{image} ../_static/img/tutorial/Artix-7_port.png
:alt: Artix-7_port
:width: 100%
:align: center
```

<br>完成 I/O Ports 设置如下图，“I/O Std”设置为“LVCMOS33*”表明 I/O 标准是 3.3V 的 LVCMOS。

```{image} ../_static/img/tutorial/set_port.png
:alt: set_port
:width: 100%
:align: center
```

<br>点击左上方工具栏中的保存按钮，提示添加新的约束文件使综合过时失效。点击“OK”。

```{image} ../_static/img/tutorial/save_constraint.png
:alt: save_constraint
:width: 100%
:align: center
```

<br>新建 XDC 文件或选择工程中已有的 XDC 文件。选择“Create a new file”，输入“File name”，点击“OK”完成约束过程。

```{image} ../_static/img/tutorial/name_con_file.png
:alt: name_con_file
:width: 70%
:align: center
```

<br>在“Sources”窗口“Constraints”层次下可以查看新建的 XDC 文件。

```{image} ../_static/img/tutorial/con_finish.png
:alt: con_finish
:width: 100%
:align: center
```

#### 新建 XDC 文件并手动输入约束

新建约束文件。点击“Add Sources”，选择“Add or Create Constraints”，点击“Next”。

```{image} ../_static/img/tutorial/create_con_file.png
:alt: create_con_file
:width: 100%
:align: center
```

<br>设置新建的 XDC 文件，输入 XDC 文件名，点击“OK”。默认文件在工程目录下的“\blink.srcs\constrs_1\new”中。

```{image} ../_static/img/tutorial/create_con_file_name.png
:alt: create_con_file_name
:width: 70%
:align: center
```

<br>完成新建 XDC 文件，点击“Finish”。

```{image} ../_static/img/tutorial/create_con_file_finish.png
:alt: create_con_file_finish
:width: 100%
:align: center
```

<br>在“Sources”窗口“Constraints”下双击打开新建好的 XDC 文件“blink_xdc.xdc”，并参照下面的约束文件格式，输入相应的 FPGA 管脚约束信息和电平标准。

```{image} ../_static/img/tutorial/source_constraint_file.png
:alt: source_constraint_file
:width: 70%
:align: center
```

<br>约束文件参考如下。

```
set_property PACKAGE_PIN AC19 [get_ports clk]
set_property PACKAGE_PIN Y3 [get_ports reset]

set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports reset]

set_property PACKAGE_PIN H8 [get_ports {leds[7]}]
set_property PACKAGE_PIN G8 [get_ports {leds[6]}]
set_property PACKAGE_PIN F7 [get_ports {leds[5]}]
set_property PACKAGE_PIN A4 [get_ports {leds[4]}]
set_property PACKAGE_PIN A5 [get_ports {leds[3]}]
set_property PACKAGE_PIN A3 [get_ports {leds[2]}]
set_property PACKAGE_PIN D5 [get_ports {leds[1]}]
set_property PACKAGE_PIN H7 [get_ports {leds[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {leds[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[0]}]


```

### 工程实现

在“Flow Navigator”中点击“Program and Debug”下的“Generate Bitstream”选项，工程会自动完成综合、布局布线、Bit 文件生成过程，完成之后，可点击“Open Implemented Design”来查看工程实现结果。

```{image} ../_static/img/tutorial/gen_bit.png
:alt: gen_bit
:width: 70%
:align: center
```

<br>提示综合过时重新运行综合和实现，点击“Yes”。在弹出的“Launch Runs”窗口点“OK”。

```{image} ../_static/img/tutorial/gen_bit_syn_od.png
:alt: gen_bit_syn_od
:width: 70%
:align: center
```

<br>在比特流文件生成完成的窗口选择“Open Hardware Manager”，进入硬件管理界面。连接 FPGA 开发板的电源线和与电脑的下载线，打开 FPGA 电源。

```{image} ../_static/img/tutorial/open_hd.png
:alt: open_hd
:width: 70%
:align: center
```

<br>在“Hardware Manager”窗口的提示信息中，点击“Open target”的下拉菜单的“Open New Target”（或在“Flow Navigator”下“Program and Debug”中展开“Open Hardware Manager”，点击“Open Target”->“Open New Target”）。也可以选择“Auto Connect”自动连接器件。

```{image} ../_static/img/tutorial/hd_open_target.png
:alt: hd_open_target
:width: 100%
:align: center
```

<br>在“Open Hardware Target”向导中，先点击“Next”，进入 Server 选择向导。

```{image} ../_static/img/tutorial/hd_open_target_1.png
:alt: hd_open_target_1
:width: 100%
:align: center
```
<br>选择连接到“Local server”，点击“Next”。

```{image} ../_static/img/tutorial/hd_open_target_2.png
:alt: hd_open_target_2
:width: 100%
:align: center
```
<br>选择目标硬件，点击“Next”。

```{image} ../_static/img/tutorial/hd_open_target_3.png
:alt: hd_open_target_3
:width: 100%
:align: center
```

<br>完成目标硬件打开。点击“Finish”。

```{image} ../_static/img/tutorial/hd_open_target_4.png
:alt: hd_open_target_4
:width: 100%
:align: center
```

<br>对目标硬件编程。在“Hardware”窗口右键单击目标器件“xc7a200t_0”，选择“Program Device…”。或者“Flow Navigator”窗口中“Program and Debug”->“Hardware Manager”->“Program Device”。

```{image} ../_static/img/tutorial/hd_program_device.png
:alt: hd_program_device
:width: 100%
:align: center
```

<br>选择下载的 bit 流文件，点击“Program”。

```{image} ../_static/img/tutorial/hd_program_bit.png
:alt: hd_program_bit
:width: 70%
:align: center
```

<br>完成下载后，“Hardware”窗口下的“xc7a200t_0”的状态变成“Programmed”。

```{image} ../_static/img/tutorial/hd_programmed.png
:alt: hd_programmed
:width: 70%
:align: center
```

<br>FPGA 开发板上， 可以看到8 个 led 灯 LED1~LED8 实现了流水灯的效果。设计完成。

```{image} ../_static/img/tutorial/hd_result.png
:alt: hd_result
:width: 100%
:align: center
```

### FPGA 在线调式说明

在 FPGA 开发过程中，经常碰到仿真上板行为不一致，更多时候是仿真正常，上板异常。由于上板调试手段薄弱，导致很难定位错误。这时候可以借助 Xilinx 的下载线进行在线调试，在线调试是在 FPGA 上运行过程中探测预定好的信号，然后通过 USB 编程线缆显示到上位机上。

本节给出简单使用在线调试的方法：RTL 里设定需探测的信号，综合并建立 Debug core，实现并生产 bit 流文件，下载 bit 流和 debug 文件，上板观察。

#### 抓取需探测的信号

在 RTL 源码中，给想要抓取的探测信号声明前增加(*mark_debug = "true"*)。

仍然以流水灯实验为例，我们想要在 FPGA 板上观察 clk 信号、reset 信号、led寄存器以及cnt计数器，需要在代码里进行如下修改。

```verilog
// blink_tb.v
`timescale 1ns / 1ps

module blink (
    (*mark_debug = "true"*)input       clk  ,
    (*mark_debug = "true"*)input       reset,

    output [7:0] leds
);

    (*mark_debug = "true"*)reg [7 :0] leds_reg;
    (*mark_debug = "true"*)reg [26:0] cnt;

    ...

endmodule
```

#### 综合并建立 debug

在综合完成后，需建立 debug。

点击工程左侧 synthesis->Open Synthesized Desgin->Set Up Debug。

```{image} ../_static/img/tutorial/set_up_debug.png
:alt: set_up_debug
:width: 70%
:align: center
```

<br>随后会出现如下界面，点击 Next。

```{image} ../_static/img/tutorial/set_up_debug_1.png
:alt: set_up_debug_1
:width: 100%
:align: center
```

<br>随后会列出抓取的 Debug 信息，点击 Next。若出现如下图所示的 Clock Domain 为`undefined`的情况，则需要按图中所示手动进行配置。

```{image} ../_static/img/tutorial/set_up_debug_2.png
:alt: set_up_debug_2
:width: 100%
:align: center
```

<br>选择抓取的深度和触发控制类型，点击 Next（更高级的调试可以勾选“Advanced trigger”）。

```{image} ../_static/img/tutorial/set_up_debug_3.png
:alt: set_up_debug_3
:width: 100%
:align: center
```

<br>最后，点击 Finish。

```{image} ../_static/img/tutorial/set_up_debug_4.png
:alt: set_up_debug_4
:width: 100%
:align: center
```

#### 实现并生产 bit 流文件

完成上一节后会出现类似下图界面，直接点击 Generate Bitstream。

```{image} ../_static/img/tutorial/debug_gen_bit.png
:alt: debug_gen_bit
:width: 100%
:align: center
```

<br>弹出如下界面，点击 Save。

```{image} ../_static/img/tutorial/debug_save_project.png
:alt: debug_save_project
:width: 70%
:align: center
```

<br>如果有后续弹出界面，继续点击 OK 或 Yes 即可。这时就进入后续生产 bit 文件的流程了，此时 Vivado 界面里的 synthesis design 界面就可以关闭了。如果发现以下错误，则是因为路径太深，引用起来名字太长，降低工程目录深度即可。

```{image} ../_static/img/tutorial/path_too_long.png
:alt: path_too_long
:width: 100%
:align: center
```

#### 下载 bit 流和 debug 文件

在完成上一节后，会生成 bit 流文件和调试使用 ltx 文件。这里，打开 Open Hardware Manager，连接好 FPGA 开发板后，选择 Program device，如下图。自动加载了 bit 流文件和调试的 ltx 文件。选择 Program，等待下载完成。

```{image} ../_static/img/tutorial/debug_hd_program_bit.png
:alt: debug_hd_program_bit
:width: 70%
:align: center
```

#### 上板观察

在下载完成后，vivado 界面如下，在线调试就是在 hw_ila_1 界面里进行的。

```{image} ../_static/img/tutorial/hd_window.png
:alt: hd_window
:width: 100%
:align: center
```

<br>hw_ila_1 界面主要分为 3 个界面，分别如下。

```{image} ../_static/img/tutorial/debug_hd_window_introduction.png
:alt: debug_hd_window_introduction
:width: 100%
:align: center
```

<br>首先，我们需要在右下角区域设定触发条件。所谓触发条件，就是设定该条件满足时获取波形，比如我先设定触发条件是led寄存器到达`8'b11101111`。在下图中，先点击“+”，在双击 leds_reg。然后如图设定触发条件。

```{image} ../_static/img/tutorial/debug_hd_trigger_setup.png
:alt: debug_hd_trigger_setup
:width: 100%
:align: center
```

<br>此外可以设定多个触发条件，比如，再加一个除法条件是写回使能是 0xf，可以设定多个触发条件直接的关系，比如是任意一个满足、两个都是满足等等，如下图。

```{image} ../_static/img/tutorial/debug_hd_trigger_setup_1.png
:alt: debug_hd_trigger_setup_1
:width: 100%
:align: center
```

<br>在左下角窗口，选择 settings，可以设定 Capture 选项，可能经常用到的是 Trigger position in window，用来设定触发条件满足的时刻在波形窗口的位置。比如，下图设定为 500，当触发条件满足时，波形窗口的第 500 个 clk 的位置是该条件，言下之意，将触发条件满足前的 500 个 clk 的信号值也抓出来了，这样可以看到触发条件之前的电路行为。Refresh rate 设定了波形窗口的刷新频率。

```{image} ../_static/img/tutorial/debug_hd_trigger_setup_2.png
:alt: debug_hd_trigger_setup_2
:width: 70%
:align: center
```

<br>触发条件建立后，就可以启动波形抓取了，最关键的有三个触发按键，即下图圈出的 3 个按键：

* 左起第一个，设定触发模式，有两个选项：单触发；循环触发。当该按键按下时，表示循环检测触发，那么只要触发条件满足，波形窗口就会更新。当设置为单触发时，就是触发一次完成后，就不会再检测触发条件了。
* 左起第二个，等待触发条件被满足。点击该按键，就是等待除法条件被满足，展示出波形。
* 左起第三个，立即触发。点击该按键，表示不管触发条件，立即抓取一段波形展示到窗口中。

```{image} ../_static/img/tutorial/debug_hd_trigger_setup_3.png
:alt: debug_hd_trigger_setup_3
:width: 100%
:align: center
```

<br>剩下的 debug 过程，就和仿真 debug 类似了，去观察波形。但在线调试时，你无法添加未被添加debug mark 的信号。在线调试过程中，可能需要不停的更换触发条件，不停的按复位键。

#### 注意事项

在添加要抓取的信号时，不要给太多信号标注 debug mark 了。在线调试时抓取波形是需要消耗电路资源和存储单元的，因而能抓取的波形大小是受限的。应当只给必要的 debug 信号添加 debug mark。

在设置抓取波形的深度时，不宜太深。如果设定得太深，那么会存在存储资源不够，导致最后生成 bit 流和 debug 文件失败。

抓取的信号数量和抓取的深度是一对矛盾的变量。如果抓取深度相对较低，抓取的信号数量就可以相对多些。
相对仿真调试，在线调试对调试思想和技巧有更高的要求，请好好整理思路，多多总结技巧。

特别强调以下几点：
* 触发条件的设定有很多组合，请根据需求认真考虑，好好设计。
* 通常只需要使用单触发模式，但循环触发有时候也很有用，必要时好好利用。
* 在线调试界面里很多按键，请自行学习，可以网上搜索资料，xilinx 官网上搜索，查找官方文档等。

最后再提醒一点，仿真通过但上板失败时，请先重点排查其他问题，最后再使用在线调试的方法。也就是仿
真通过，上板异常时，应按以下流程排查：
1) 复核生成、下载的 bit 文件是否正确。
2) 复核仿真结果是否正确。
3) 检查实现时的时序报告（Vivado 界面左侧“
Timing Summary”）。
4) 认真排查综合和实现时的 Warning
5) 排查 RTL 代码规范，避免多驱动、阻塞赋值乱用。
6) 使用 Vivado 的逻辑分析仪进行板上在线调试

