# 参考资料
## MIPS手册

* [MIPS Volume I - Introduction to the MIPS32 Architecture](https://www.mips.com/?do-download=introduction-to-the-mips32-architecture-v6-01)
* [MIPS Volume II - The MIPS32 Instruction Set](https://www.mips.com/?do-download=the-mips32-instruction-set-v6-06)
* [MIPS Volume III - The MIPS32 and microMIPS32 Privileged Resource Architecture](https://www.mips.com/?do-download=the-mips32-and-micromips32-privileged-resource-architecture-v6-02)

## 实验板手册

* 龙芯体系结构实验箱（Artix-7）原理图
* 龙芯体系结构实验箱（Artix-7）引脚列表

## CPU设计

* 雷思磊 《自己动手写CPU》 电子工业出版社
* 汪文祥，邢金璋《CPU设计实战》 机械工业出版社
