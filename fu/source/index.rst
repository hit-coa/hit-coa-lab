.. Copyright 2021, Harbin Institute of Technology

计算机组织与体系结构（2024秋）- 未来技术
=======================================

Git仓库：`hit-coa-lab <https://gitlab.com/hit-coa/hit-coa-lab>`__

-----

实验课时间：以课上为准

实验分数构成：以课上为准

注意：本课程实验要求使用vivado 2019.2版本！

-----

.. used to get the title of TOC right in generated pdf
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: 目录

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 教程

   tutorial/verilog
   tutorial/dev_env
   tutorial/dev_step

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 实验

   labs/overview
   labs/lab1
   labs/lab2_2024
   labs/lab3_2024
   labs/lab-extra1
   labs/lab-extra2
   labs/lab-extra3
   labs/lab-extra4

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 附录

   appendix/faqs
   appendix/controlmode
   appendix/reference
   appendix/ip_core
   appendix/simulator
